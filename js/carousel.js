$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    autoplayTimeout:3000,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
});
$(document).ready(function(){
const showMsg = localStorage.getItem('showMsg');

if(showMsg != 'false'){
  $('.alert').show();
}
else { 
    $('.alert').hide();
}

$('.closebtn').on('click', function(){
  $('.alert').fadeOut('slow');
  localStorage.setItem('showMsg', 'false');
});

$('.abc').on('click', function(){
  localStorage.setItem('showMsg', null);
});
});